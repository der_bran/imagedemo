package at.bran.imagedemo;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;


public class MainActivity extends Activity {

    private static final int REQUEST_CHOOSE_IMAGE = 1;
    private ImageView imageView;
    private TextView textView;
    private Bitmap currentBitmap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.imageView = (ImageView) findViewById(R.id.imageView);
        this.textView = (TextView) findViewById(R.id.textView);

        Button chooseImageButton = (Button) findViewById(R.id.chooseImageButton);
        chooseImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(i, REQUEST_CHOOSE_IMAGE);
            }
        });

        imageView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if(event.getAction() == MotionEvent.ACTION_UP) {
                    // calculate inverse matrix (image matrix contains scale factor in [0,1])
                    Matrix inverse = new Matrix();
                    imageView.getImageMatrix().invert(inverse);

                    // map touch point from ImageView coordinates to image coordinates
                    float[] touchPoint = new float[] {event.getX(), event.getY()};
                    inverse.mapPoints(touchPoint);
                    textView.setText(String.format("last touch point: (%s / %s)", touchPoint[0], touchPoint[1]));
                    drawPoint(touchPoint[0], touchPoint[1], inverse);
                }
                return true;
            }
        });
    }

    private void drawPoint(float x, float y, Matrix inverse) {
        // map circle radius to coordinate system of image
        float circleRadius = inverse.mapRadius(20.0f);
        float strokeWidth = inverse.mapRadius(5.0f);

        // create a new Bitmap from current one and draw on Canvas
        Bitmap bitmap = currentBitmap.copy(Bitmap.Config.RGB_565, true);
        Canvas canvas = new Canvas(bitmap);
        Paint paint = new Paint();
        paint.setColor(Color.RED);
        paint.setStrokeWidth(strokeWidth);
        paint.setStyle(Paint.Style.STROKE);
        canvas.drawCircle(x, y, circleRadius, paint);

        this.currentBitmap = bitmap;
        imageView.setImageBitmap(bitmap);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CHOOSE_IMAGE && resultCode == RESULT_OK && data != null) {
            // load image
            Uri imageUri = data.getData();
            String[] filePathColumn = { MediaStore.Images.Media.DATA };
            Cursor cursor = getContentResolver().query(imageUri, filePathColumn, null, null, null);
            cursor.moveToFirst();
            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String imagePath = cursor.getString(columnIndex);
            cursor.close();
            this.currentBitmap = BitmapFactory.decodeFile(imagePath);

            // update ImageView
            this.imageView.setImageBitmap(currentBitmap);
        }
    }
}
